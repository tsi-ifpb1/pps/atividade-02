package controller;

import java.util.ArrayList;
import java.util.Random;

public class PontuacaoController {
	
	private ArrayList<Inimigo> inimigos = new ArrayList<Inimigo>();
	private Pontuacao pontuacao;
	private int life = 1000;
	
	
	public PontuacaoController() {		
		addInimigos(new Inimigo(5, 8, 8, "Soldado de Guerrilha"));
		addInimigos(new Inimigo(10, 7, 10, "Espi�es"));
		addInimigos(new Inimigo(2, 7, 10, "Ex�rcito Inimigo"));
		this.pontuacao = new Pontuacao();
	}
	
	public void ataque() {
		while(getLife() > 0) {
			Random rd = new Random();
			int enemy = rd.nextInt(3);
			
			while(getLife() > 0) {
				int ataque = rd.nextInt(10);
				int defesa = rd.nextInt(10);
				int estrategia = rd.nextInt(10);
				
				if(ataque > getInimigos().get(enemy).getForca()) {
					getPontuacao().derrotou(getInimigos().get(enemy).getTipo());
					System.out.println("Derrotou " + getInimigos().get(enemy).getTipo() + "!");
					break;
				} else {
					if(getInimigos().get(enemy).getEstrategia() > estrategia) {
						int dano = (getInimigos().get(enemy).getForca() - defesa);
						if(dano > 0) {
							life -= dano;
							System.out.println("Levou " + dano + " pontos de dano!");
						}
					}
					
				}
				
			}
			
		}
		System.out.println("Pontua��o m�xima: " + getPontuacao().getPontos() + " pontos.");
	}

	public ArrayList<Inimigo> getInimigos() {
		return inimigos;
	}

	public void addInimigos(Inimigo inimigo) {
		this.inimigos.add(inimigo);
	}

	public Pontuacao getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(Pontuacao pontuacao) {
		this.pontuacao = pontuacao;
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}
	
	
	
	

}
