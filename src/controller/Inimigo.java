package controller;

public class Inimigo {
	
	private String tipo;
	private int inteligencia;
	private int forca;
	private int estrategia;
	
	public Inimigo() {
		
	}
	
	public Inimigo(int inteli, int forca, int estrat, String tipo) {
		this.inteligencia = inteli;
		this.forca = forca;
		this.estrategia = estrat;
		this.tipo = tipo;
	}

	public int getInteligencia() {
		return inteligencia;
	}

	public void setInteligencia(int inteligencia) {
		this.inteligencia = inteligencia;
	}

	public int getForca() {
		return forca;
	}

	public void setForca(int forca) {
		this.forca = forca;
	}

	public int getEstrategia() {
		return estrategia;
	}

	public void setEstrategia(int estrategia) {
		this.estrategia = estrategia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
