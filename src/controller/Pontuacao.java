package controller;

public class Pontuacao {
	private int pontos;
	
	public Pontuacao() {
		this.pontos = 0;
	}
	
	public void derrotou(String tipo) {
		if(tipo.equals("Soldado de Guerrilha")) {
			this.setPontos(100);
		} else if(tipo.equals("Espi�es")) {
			this.setPontos(200);
		} else {
			this.setPontos(50);
		}
	}
	
	public int getPontos() {
		return this.pontos;
	}
	
	public void setPontos(int ponto) {
		this.pontos += ponto;
	}

}
